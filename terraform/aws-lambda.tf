# variable "AWS_ACCESS_KEY" {}
# variable "AWS_SECRET_KEY" {}
# variable "AWS_REGION" {}

resource "aws_iam_role" "lambda_ec2_autostartstop" {
    name = "lambda_ec2_autostartstop"

    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "lambda_ec2_autostartstop_policy" {
    name = "lambda_ec2_autostartstop_policy"
    role = "${aws_iam_role.lambda_ec2_autostartstop.id}"

    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:StartInstances",
                "ec2:StopInstances",
                "ec2:DescribeRegions",
                "ec2:DescribeInstances",
                "lambda:InvokeFunction"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

data "archive_file" "lambda_zip" {
    type        = "zip"
    source_dir  = "../ec2-autostartstop/"
    output_path = "ec2-autostartstop.zip"
}

resource "aws_lambda_function" "lambda_ec2_autostartstop" {
    filename = "ec2-autostartstop.zip"
    function_name = "ec2-autostartstop"
    role = "${aws_iam_role.lambda_ec2_autostartstop.arn}"
    handler = "ec2-autostartstop.lambda_handler"
    source_code_hash = "${data.archive_file.lambda_zip.output_base64sha256}"
    runtime = "python3.6"
    memory_size = 256
    timeout = 300
}

resource "aws_cloudwatch_event_rule" "lambda_ec2_autostartstop" {
    name = "lambda_ec2_autostartstop"
    description = "Run every 5 min"
    schedule_expression = "rate(5 minutes)"
}

resource "aws_cloudwatch_event_target" "lambda_ec2_autostartstop" {
    rule = "${aws_cloudwatch_event_rule.lambda_ec2_autostartstop.name}"
    target_id = "ec2-autostartstop"
    arn = "${aws_lambda_function.lambda_ec2_autostartstop.arn}"
}

resource "aws_lambda_permission" "lambda_ec2_autostartstop" {
    statement_id = "AllowExecutionFromCloudWatch"
    action = "lambda:InvokeFunction"
    function_name = "${aws_lambda_function.lambda_ec2_autostartstop.function_name}"
    principal = "events.amazonaws.com"
    source_arn = "${aws_cloudwatch_event_rule.lambda_ec2_autostartstop.arn}"
}
